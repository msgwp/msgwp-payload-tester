<?php
/*
 * Plugin Name: msgWP Payload Tester
 * Description: Forcefully log all POSTS requests on "/wp-json/msgWP/v1/telegram".
 * Plugin URI:  https://msgwp.com
 * Version:     0.1
 * Author:      implenton
 * Author URI:  https://implenton.com
 * License:     GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 */

namespace msgWP\AddOn\PayloadTester;

add_action( 'rest_api_init', __NAMESPACE__ . '\overwrite_telegram_endpoint', 999 );

function overwrite_telegram_endpoint() {
    register_rest_route( 'msgWP/v1', '/telegram', [
        'methods'             => 'POST',
        'callback'            => function ( \WP_REST_Request $request ) {
            error_log( json_encode( json_decode( $request->get_body(), true ), JSON_PRETTY_PRINT ) );

            return new \WP_REST_Response( null, 200 );
        },
        'permission_callback' => function () {
            return true;
        }
    ], true );
}