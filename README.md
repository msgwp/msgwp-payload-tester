# msgWP Payload Tester Add-on

Use this if you want to **forcefully** log all `POSTS` requests sent to `/wp-json/msgWP/v1/telegram` and nothing else.